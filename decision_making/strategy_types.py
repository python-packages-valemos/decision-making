from dataclasses import dataclass
from abc import ABC, abstractmethod


class BaseStep(ABC):
    """each custom step must inherit BaseStep class"""

    @abstractmethod
    def get_id(self):
        """must provide hashable type with unique id for each step"""
        pass


@dataclass
class Strategy:
    steps: list[BaseStep]
    name: str = "unnamed"


@dataclass
class TreeNode:
    step: BaseStep
    options: list


@dataclass
class StepOptions:
    options: list[BaseStep]

