def pareto_dominating(first, second, criteria):
    choice1, choice2 = criteria(first), criteria(second)

    for component1, component2 in zip(choice1, choice2):
        if component1 < component2:
            return False

    return choice1 != choice2


def slater_dominating(first, second, criteria):
    choice1, choice2 = criteria(first), criteria(second)

    for component1, component2 in zip(choice1, choice2):
        if component1 <= component2:
            return False

    return choice1 != choice2


def get_optimal_solutions(alternatives, criteria, domination_func):
    alternatives = list(alternatives)
    i = 0
    j = 1

    popped_i = False

    while i < len(alternatives):
        while j < len(alternatives):
            if domination_func(alternatives[i], alternatives[j], criteria):
                alternatives.pop(j)
            elif domination_func(alternatives[j], alternatives[i], criteria):
                alternatives.pop(i)
                popped_i = True
            else:
                j += 1

        if popped_i:
            popped_i = False
        else:
            i += 1

        j = i + 1

    return alternatives


def get_pareto_optimal(alternatives, criteria):
    return get_optimal_solutions(alternatives, criteria, pareto_dominating)


def get_slater_optimal(alternatives, criteria):
    return get_optimal_solutions(alternatives, criteria, slater_dominating)
