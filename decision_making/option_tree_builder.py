from typing import Union
from .strategy_types import BaseStep, TreeNode


class OptionTreeBuilder:
    """
    provides an interface to build decision tree
    """

    def __init__(self):
        self._step_map: dict[str, BaseStep] = {}
        self._tree_node_map: dict[str, TreeNode] = {}
        self._tree: TreeNode = None

    def get_tree(self) -> TreeNode:
        return self._tree

    def add_branches(self, step: BaseStep, branches: list[Union[BaseStep, TreeNode]]):
        for next_step in branches:
            self.add_pair(step, next_step)

    def add_pair(self, step: BaseStep, next_step: Union[BaseStep, TreeNode]):
        if step.get_id() not in self._step_map:
            self._add_new_node(step)

        if next_step.get_id() not in self._step_map:
            self._add_new_node(next_step)

        self._add_next_step(step, next_step)

    def _add_new_node(self, step: BaseStep):
        self._step_map[step.get_id()] = step
        self._tree_node_map[step.get_id()] = TreeNode(step, [])
        if self._tree is None:
            self._tree = self._tree_node_map[step.get_id()]

    def _add_next_step(self, step: BaseStep, next_step: Union[BaseStep, TreeNode]):
        assert issubclass(step.__class__, BaseStep)
        assert issubclass(next_step.__class__, BaseStep) or isinstance(next_step, TreeNode)

        if next_step.get_id() not in self._step_map:
            self._add_new_node(next_step)
            return

        step_node = self._tree_node_map[step.get_id()]
        next_node = self._tree_node_map[next_step.get_id()]
        if next_step not in step_node.options:
            step_node.options.append(next_node)

    def _update_step(self, step: BaseStep):
        self._step_map[step.get_id()] = step
