from secrets import choice
from .strategy_types import *
from copy import deepcopy, copy
from typing import Union


def get_tree_strategies(strategy_tree: TreeNode, strategy: Strategy = None, strategy_class=Strategy):
    """
    strategy_tree is a collection of steps,
    that refer to each other in tree like manner using StrategyNode
    branches can be joined using the same strategy node in options
    User must avoid cycles to get results
    """
    
    if strategy is None:
        strategy = strategy_class([strategy_tree.step])

    if len(strategy_tree.options) == 0:
        yield strategy

    for subtree in strategy_tree.options:
        if subtree.step not in strategy.steps:
            next_strategy = deepcopy(strategy)
            next_strategy.steps.append(subtree.step)
            yield from get_tree_strategies(subtree, next_strategy)
        else:
            raise RecursionError("node cycle detected")


def _unwrap_step_set(step_set: Union[BaseStep, StepOptions, TreeNode], strategy_class=Strategy):
    if isinstance(step_set, BaseStep):
        yield step_set
    elif isinstance(step_set, list):
        yield from step_set
    elif isinstance(step_set, StepOptions):
        yield from step_set.options
    elif isinstance(step_set, TreeNode):
        yield from get_tree_strategies(step_set, strategy_class)
    else:
        raise ValueError(f"unsupported type to unwrap {type(step_set)}")


def get_sequential_strategies(choices: list[Union[BaseStep, StepOptions, TreeNode]], steps=None, strategy_class=Strategy):
    """
    choices is a list of sequential definite choice, list of choices or tree of choices.
    If non base step encountered, 
    it will generate a new strategy for each of the options available.
    """
    
    if len(choices) == 0:
        yield strategy_class(steps)
        return
    
    if steps is None:
        steps = []
    
    for step in _unwrap_step_set(choices[0], strategy_class):
        yield from get_sequential_strategies(choices[1:], steps + [step], strategy_class)
